import { Moment } from "moment";
import { NgbDate } from "@ng-bootstrap/ng-bootstrap";

export class PaymentMap {
    dateFrom: Moment;
    dateTo: Moment;
    redDays: NgbDate[];

    dateFromNgb?: NgbDate;
    dateToNgb?: NgbDate;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
      }
}
