import {Student} from './student';
import { Moment } from 'moment';

export class Payment {
  id?: number;
  student_id: number;
  sum: number;
  per_day: number;
  paid_to: string;
  paid_from: string;
  months_info: any;
  date?: string;
  student?: Student;

  paid_to_date?: Moment;
  paid_from_date?: Moment;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
