import {Group} from './group';
import {Payment} from './payment';

export class Exercise {
  id: number;
  class_date: string;
  start_time: string;
  end_time: string;
  group_id: number;
  comments: string;

  group: Group;
  payments: Payment[];
}
