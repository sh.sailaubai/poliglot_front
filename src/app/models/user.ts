import {Group} from './group';

export class User {
  id: number;
  username: string;
  auth_key?: string;
  type: number;
  full_name: number;
  groups: Group[];
  password?: string;
}

export class AuthUser {
  username: string;
  password: string;
}
