import { Payment } from "./payment";
import { Exercise } from "./exercise";

export class Today {
    payments: Payment[];
    exercises: Exercise[];
}
