import {Exercise} from './exercise';
import {User} from './user';
import {Student} from './student';

export class Group {
  id: number;
  name: string;
  user_id: number;
  payment_for_day: number;
  mo: boolean;
  tu: boolean;
  web: boolean;
  th: boolean;
  fri: boolean;
  sat: boolean;
  sun: boolean;

  exersises?: Exercise[];
  user?: User;
  students?: Student[];

  groupSchedule?: number[];
}
