import {Payment} from './payment';
import {Group} from './group';

export class Student {
  id: number;
  name: string;
  group_id: number;
  payments: Payment[];
  group: Group;
}
