import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';


export class TokenInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.auth_key) {
      const newRequest = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.auth_key}`
        }
      });
      return next.handle(newRequest);
    }
    return next.handle(request);
  }
}
