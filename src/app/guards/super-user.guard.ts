import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../services/auth-service';

@Injectable({
  providedIn: 'root'
})
export class SuperUserGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const user = this.authService.getUser();
    if (user) {
      if (user.type !== 1) {
        this.router.navigate(['/']);
        return false;
      } else {
        return true;
      }
    }
    return false;
  }
}
