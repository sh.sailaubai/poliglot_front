import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { AuthenticateComponent } from './components/authenticate/authenticate.component';
import { MainComponent } from './components/main/main.component';
import {RouterModule} from '@angular/router';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {AuthGuard} from './guards/auth.guard';
import { TeacherPageComponent } from './components/teacher-page/teacher-page.component';
import {AuthService} from './services/auth-service';
import {ExerciseService} from './services/exercise-service';
import {GroupService} from './services/group-service';
import {PaymentService} from './services/payment-service';
import {StudentService} from './services/student-service';
import {UserService} from './services/user-service';
import { SuperUserPageComponent } from './components/super-user-page/super-user-page.component';
import {ErrorInterceptor} from './helpers/error.interceptor';
import {TokenInterceptor} from './helpers/token.interceptor';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import { SuperUserGuard } from './guards/super-user.guard';
import { ExerciseComponent } from './components/exercise/exercise.component';
import { PaymentComponent } from './components/payment/payment.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { StudentsComponent } from './components/students/students.component';
import { GroupExercisesComponent } from './components/group/group-exercises/group-exercises.component';
import { TeacherListComponent } from './components/teacher-list/teacher-list.component';
import { GroupListComponent } from './components/group-list/group-list.component';
import { StudentsListComponent } from './components/students-list/students-list.component';

const routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'exercise/:id',
    component: ExerciseComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'teachers',
    component: TeacherListComponent,
    canActivate: [SuperUserGuard]
  },
  {
    path: 'groups',
    component: GroupListComponent,
    canActivate: [SuperUserGuard]
  },
  {
    path: 'login',
    component: AuthenticateComponent
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

const customNotifierOptions: NotifierOptions = {
  position: {
		horizontal: {
			position: 'right',
			distance: 12
		},
		vertical: {
			position: 'top',
			distance: 12,
			gap: 10
		}
	},
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};

@NgModule({
  declarations: [
    AppComponent,
    AuthenticateComponent,
    MainComponent,
    TeacherPageComponent,
    SuperUserPageComponent,
    ExerciseComponent,
    PaymentComponent,
    NotFoundComponent,
    StudentsComponent,
    GroupExercisesComponent,
    TeacherListComponent,
    GroupListComponent,
    StudentsListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    NotifierModule.withConfig(customNotifierOptions)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    AuthService,
    ExerciseService,
    GroupService,
    PaymentService,
    StudentService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
