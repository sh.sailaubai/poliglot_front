import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { GroupService } from 'src/app/services/group-service';
import { Observable, Subscription } from 'rxjs';
import { Group } from 'src/app/models/group';
import { Exercise } from 'src/app/models/exercise';

@Component({
  selector: 'app-group-exercises',
  templateUrl: './group-exercises.component.html',
  styleUrls: ['./group-exercises.component.scss']
})
export class GroupExercisesComponent implements OnInit, OnDestroy {
  @Input() groupObserver: Observable<Group>;

  private subscription: Subscription;
  private subscription2: Subscription;
  private page = 1;
  public hasNext = true;
  public exercises: Exercise[];
  public group: Group;

  constructor(
    private groupService: GroupService
  ) { }

  ngOnInit() {
    this.subscription = this.groupObserver.subscribe((group: Group) => {
      this.exercises = [];
      this.page = 1;
      this.group = group;
      this.getExercises();
    });
  }

  getExercises(): void {
    this.subscription2 = this.groupService.getExersices(this.group.id, this.page).subscribe(
      (exercises: Exercise[]) => {
        this.exercises = this.exercises.concat(exercises);
        if (exercises.length > 0) {
          this.page++;
        } else {
          this.hasNext = false;
        }
      }
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) this.subscription.unsubscribe();
    if (this.subscription2) this.subscription2.unsubscribe();
  }

}
