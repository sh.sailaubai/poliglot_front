import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user-service';
import { Subscription, combineLatest } from 'rxjs';
import { Payment } from 'src/app/models/payment';
import { Group } from 'src/app/models/group';
import { DateService } from 'src/app/services/date.service';
import { NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-teacher-list',
  templateUrl: './teacher-list.component.html',
  styleUrls: ['./teacher-list.component.scss']
})
export class TeacherListComponent implements OnInit, OnDestroy {

  teachers: User[];
  subscription: Subscription;
  subscription2: Subscription;
  loading = true;
  teacher: User;
  monthsList: string[];
  yearsList: number[];

  month: number;
  year: number;

  dateModel: NgbDate;

  teacherPayment: number;

  teacherGroups: Group[];

  teacherPayments: Payment[];

  constructor(
    private userService: UserService,
    private dateService: DateService
  ) { }

  ngOnInit() {
    this.subscription = this.userService.getTeachers().subscribe((teachers) => {
      teachers.forEach((teacher: User) => {
        teacher.groups.forEach((group: Group) =>{
          this.userService.dateService.weekList.forEach((w) => {
            group[w] = group[w] === "1"
          });
        })
      });
      this.teachers = teachers; 
      this.loading = false
    });
    this.monthsList = this.dateService.monthsListRu;
    this.yearsList = this.dateService.availableYears;
    this.month = this.dateService.currentMonth();
    this.year = this.dateService.currentYear();
    this.dateModel = new NgbDate(this.year, this.month, 1);
  }

  selectTeacher(teacher: User): void {
    this.teacher = teacher;
    this.teacherPayments = [];
    this.teacherGroups = [];
    this.latestSubscribe(teacher, this.year, this.month);
  }

  selectDate() {
    this.year = this.dateModel.year;
    this.month = this.dateModel.month;
    this.teacherPayments = [];
    this.teacherGroups = [];
    this.latestSubscribe(this.teacher, this.year, this.month);
  }

  latestSubscribe(teacher: User, year: number, month?: number): void {
    this.subscription2 = combineLatest(
      this.userService.getPayments(teacher.id, year, month),
      this.userService.getGroupsWithExercises(teacher.id, year, month)
    ).subscribe(([payments, groups]) => {
      this.getPayments(payments, month);
      this.getTeacherGroups(groups);
    })
  }

  getTeacherGroups(groups): void {
    this.teacherGroups = groups;
  }

  getPayments(payments, month): void {
    let sum = 0;
    this.teacherPayment = 0;
    payments.forEach((payment: Payment) => {
      payment.months_info = JSON.parse(payment.months_info);
      if (payment.months_info.hasOwnProperty(month))
      {
        sum += Number(payment.months_info[month]);
      }
    });
    this.teacherPayment = sum / 2;
  }

  ngOnDestroy(): void {
    if (this.subscription) this.subscription.unsubscribe();
    if (this.subscription2) this.subscription2.unsubscribe();
  }

}
