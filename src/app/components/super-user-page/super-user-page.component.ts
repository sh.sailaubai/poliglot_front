import { Component, OnInit } from '@angular/core';
import { DateService } from 'src/app/services/date.service';
import { StudentService } from 'src/app/services/student-service';
import { GroupService } from 'src/app/services/group-service';
import { UserService } from 'src/app/services/user-service';
import { Subscription, combineLatest } from 'rxjs';
import { Group } from 'src/app/models/group';
import { User } from 'src/app/models/user';
import { NotifierService } from 'angular-notifier';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Week } from 'src/app/shared/interfaces/week';
import { Student } from 'src/app/models/student';
import { Today } from 'src/app/models/today';

@Component({
  selector: 'app-super-user-page',
  templateUrl: './super-user-page.component.html',
  styleUrls: ['./super-user-page.component.scss']
})
export class SuperUserPageComponent implements OnInit {

  subscription: Subscription;
  subscription2: Subscription;

  public groupForm: FormGroup;
  public teacherForm: FormGroup;
  public studentForm: FormGroup;

  groups: Group[];
  teachers: User[];
  students: Student[];
  today: Today;

  weekRus: Week[];

  loading = true;
  constructor(
    private dateService: DateService,
    private studentService: StudentService,
    private groupService: GroupService,
    private userService: UserService,
    private notifier: NotifierService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.groupForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      user_id: [null, Validators.required],
      payment_for_day: [0, [Validators.required, Validators.min(1)]],
      mo: [false],
      tu: [false],
      wed: [false],
      th: [false],
      fri: [false],
      sat: [false],
      sun: [false],
    });
    this.teacherForm = this.formBuilder.group({
      full_name: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      type: [2, Validators.required]
    });
    this.studentForm = this.formBuilder.group({
      name: ['', Validators.required],
      group_id: [null, Validators.required]
    });
    this.weekRus = this.dateService.weekListRus;
    this.subscription = combineLatest(
      this.groupService.getAll(),
      this.userService.getTeachers(),
      this.studentService.getAll(),
      this.userService.getToday()
    ).subscribe(([groups, teachers, students, today]) => {
      this.groups = groups;
      this.teachers = teachers;
      this.students = students;
      this.today = today;
      this.loading = false;
    })
  }

  onSubmitGroup() {
    const values = this.groupForm.value;
    if (this.groupForm.invalid) {
      return;
    }
    if (values.id) {
      this.updateGroup(values);
    } else {
      this.addGroup(values);
    }   
  }

  onSubmitTeacher(): void {
    const values = this.teacherForm.value;
    if (this.teacherForm.invalid) {
      return;
    }
    this.userService.addTeacher(values).subscribe((data: User) => {
      this.teachers.push(data);
      this.teacherForm.reset();
      this.showSuccess('Преподаватель добавлен');
    }, (error) => {
      this.showError('Ошибка при сохранений, попробуите еще раз');
    })
  }

  onSubmitStudent(): void {
    if (this.studentForm.invalid) {
      return;
    }
    this.studentService.addStudent(this.studentForm.value).subscribe((student) => {
      this.students.push(student);
      this.studentForm.reset();
      this.showSuccess('Ученик добавлен');
    }, (error) => {
      this.showError('Ошибка при сохранений, попробуите еще раз');
    }
    )
  }

  addGroup(group: Group): void {
    this.groupService.addGroup(group).subscribe(
      (data) => {
        this.groups.push(data);
        this.groupForm.reset();
        this.showSuccess('Группа добавлена');
      },
      (error) => {
        this.showError('Ошибка при сохранений, попробуите еще раз');
      }
    );
  }

  selectForUpdate(group: Group): void {
    for (let prop in group) {
      this.groupForm.controls[prop].setValue(group[prop]);
    }
  }

  updateGroup(group: Group): void {
    this.groupService.updateGroup(group).subscribe(
      (data) => {
        const indx = this.groups.findIndex((_group) => _group.id === group.id);
        if (indx > -1) {
          this.groups[indx] = data;
        }
        this.groupForm.reset();
        this.showSuccess(`Группа "${group.name}" отредактированна`);
      },
      (error) => {
        this.showError('Ошибка при сохранений, попробуите еще раз');
      }
    );
  }

  showError(message: string): void {
    this.notifier.notify('error', message);
  }

  showSuccess(message: string): void {
    this.notifier.notify('success', message);
  }

}
