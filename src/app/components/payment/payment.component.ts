import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Student } from 'src/app/models/student';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { DateService } from 'src/app/services/date.service';
import { Group } from 'src/app/models/group';
import { StudentService } from 'src/app/services/student-service';
import { Payment } from 'src/app/models/payment';
import { Subscription, Observable } from 'rxjs';
import { PaymentMap } from 'src/app/models/payment-map';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit, OnDestroy {
  @Input() student: Student;

  subscription2: Subscription;

  fromDate: NgbDate;
  toDate: NgbDate;
  hoveredDate: NgbDate;

  loading = true;

  paymentsToDay = 0;
  modulo = 0;

  dayCount = 0;
  sumForPaid = 0;

  startDate: NgbDate;

  payments: Payment[];

  paymentsMap: PaymentMap;

  constructor(
    private dateService: DateService,
    private studentService: StudentService
  ) {}

  ngOnInit() {    
    this.init();
  }

  init() {
    this.subscription2 = this.studentService.getPayments(this.student.id).subscribe(
      (payments) => {
        this.payments = payments;
        this.calculatePayments();
        this.startDate = this.paymentsMap.dateToNgb; 
        this.loading = false;
      }
    );
  }

  calculatePayments(): void {
    this.paymentsMap = this.studentService.paymentDateRange(this.payments);
  }

  reverseDate(toDate: NgbDate): NgbDate {
    let momentDate = this.dateService.ngbDateToMoment(toDate);
    momentDate.subtract(1, "days")
    while(!this.student.group.groupSchedule.includes(momentDate.day())) {
      momentDate.subtract(1, "days")
    }
    return this.dateService.momentToNgb(momentDate);
  }

  // suitableFromDate(toDate: NgbDate): NgbDate {
  //   let momentDate = this.dateService.ngbDateToMoment(toDate);
  //   while(!this.groupSchedule.includes(momentDate.day())) {
  //     momentDate.add(1, "days")
  //   }
  //   return this.dateService.momentToNgb(momentDate);
  // }

  isGroupDate(date: NgbDate): boolean {
    return this.student.group.groupSchedule.includes(this.dateService.ngbDateToMoment(date).day());
  }

  isNextSelectDate(date: NgbDate): boolean {
    if (this.paymentsMap.dateToNgb) {
      const prevPaymentDay = this.reverseDate(date);
      return prevPaymentDay.equals(this.paymentsMap.dateToNgb);
    }
    return true;
  }

  calculatePayment(sum: number): void {
    this.paymentsToDay = Math.trunc(sum/this.student.group.payment_for_day)
    this.modulo = sum - (this.paymentsToDay * this.student.group.payment_for_day)
  }

  calculatePaidSum(dayCount: number): number {
    return this.student.group.payment_for_day * dayCount;
  }

  calculateDays(): number {
    const dates = this.dateService.dateRange(this.fromDate, this.toDate);
    return dates.reduce((accumulator ,date) => {
      return this.student.group.groupSchedule.includes(date.getDay()) ? accumulator+1 : accumulator
    }, 0);
  }

  onDateSelection(date: NgbDate) {
    if (this.isGroupDate(date)) {
      if (!this.fromDate && !this.toDate && this.isNextSelectDate(date)) {
        this.fromDate = date;
      } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
        this.toDate = date;
        this.dayCount = this.calculateDays();
        this.sumForPaid = this.calculatePaidSum(this.dayCount);
      } else {
        this.toDate = null;
      }
    }    
  }

  isHovered(date: NgbDate): boolean {
    const hovered = this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    return hovered && this.isGroupDate(date);
  }

  isInside(date: NgbDate): boolean {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isDisabled(date: NgbDate) {
    return !this.isGroupDate(date);
  }

  isRange(date: NgbDate): boolean {
    if (this.isGroupDate(date)) {
      return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
    } 
    return false;
  }

  isLastDay(date: NgbDate): boolean {
    if (this.toDate) {
      return this.toDate.equals(date);
    } else if (this.paymentsMap.dateToNgb) {
      return this.paymentsMap.dateToNgb.equals(date);
    }
    return false;    
  }

  inPaymentMap(date: NgbDate): boolean {
    if (this.paymentsMap.dateToNgb && this.paymentsMap.dateFromNgb) {
      if (this.isGroupDate(date)) {
        return (date.before(this.paymentsMap.dateToNgb) && date.after(this.paymentsMap.dateFromNgb)) || date.equals(this.paymentsMap.dateFromNgb) || date.equals(this.paymentsMap.dateToNgb);
      }
    }    
    return false;
  }

  savePayments(): void {
    let payment = new Payment().deserialize({
      student_id: this.student.id,
      sum: this.sumForPaid,
      per_day: this.student.group.payment_for_day,
      paid_from: this.dateService.dtToString(this.fromDate),
      paid_to: this.dateService.dtToString(this.toDate),
      months_info: JSON.stringify(
        this.dateService.dateRangeInfo(
          this.fromDate,
          this.toDate,
          this.student.group.groupSchedule,
          this.student.group.payment_for_day
        )
      )
    });
    this.studentService.addPayments(payment).subscribe((data: Payment) => {
      this.payments.push(data);
      this.cancel();
      this.calculatePayments();
    });
  }

  cancel(): void {
    this.fromDate = null;
    this.toDate = null;
    this.dayCount = 0;
    this.sumForPaid = this.calculatePaidSum(this.dayCount);
  }

  ngOnDestroy(): void {
    if (this.subscription2) this.subscription2.unsubscribe();
  }

}
