import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExerciseService } from 'src/app/services/exercise-service';
import { PaymentService } from 'src/app/services/payment-service';
import { ActivatedRoute, Router } from '@angular/router';
import { Exercise } from 'src/app/models/exercise';
import { Subscription, BehaviorSubject } from 'rxjs';
import { Student } from 'src/app/models/student';
import { Group } from 'src/app/models/group';

@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.scss']
})
export class ExerciseComponent implements OnInit, OnDestroy {
  exercise: Exercise;
  subscription: Subscription;
  groupId = new BehaviorSubject<number>(null);

  loading = true;

  constructor(
    private exerciseService: ExerciseService,
    private route: ActivatedRoute,
    private router: Router    
  ) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.subscription = this.exerciseService.getExercise(id).subscribe((exercise) => {
      this.exercise = exercise;
      this.groupId.next(exercise.group_id);
      this.loading = false;
    }, (error) => {
      this.router.navigate(['/404']);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
