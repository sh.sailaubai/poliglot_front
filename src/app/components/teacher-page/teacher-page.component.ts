import { Component, OnInit, OnDestroy } from '@angular/core';
import {AuthService} from '../../services/auth-service';
import {UserService} from '../../services/user-service';
import {Group} from '../../models/group';
import {combineLatest, Subscription, BehaviorSubject} from 'rxjs';
import {User} from '../../models/user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DateService} from '../../services/date.service';
import {ExerciseService} from '../../services/exercise-service';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-teacher-page',
  templateUrl: './teacher-page.component.html',
  styleUrls: ['./teacher-page.component.scss']
})
export class TeacherPageComponent implements OnInit, OnDestroy {
  public loading = true;
  public groups: Group[];
  public subscription: Subscription;
  public user: User;
  public classForm: FormGroup;
  public selectedGroup = new BehaviorSubject<Group>(null);
  public selectedGroupId = new BehaviorSubject<number>(null);

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private dateService: DateService,
    private exerciseService: ExerciseService,
    private router: Router,
    private notifier: NotifierService
  ) { }

  ngOnInit() {
    this.user = this.authService.getUser();
    this.subscription = combineLatest(
      this.userService.getGroups(this.user.id),
      this.authService.currentUser
    ).subscribe(([groups, user]) => {
      this.user = user;
      this.groups = groups;
    });
    this.classForm = this.formBuilder.group({
      class_date: ['', Validators.required],
      start_time: ['', Validators.required],
      end_time: ['', Validators.required],
      group_id: ['', Validators.required],
      comments: ['']
    });
  }

  get f() { return this.classForm.controls; }

  onSubmit() {
    const values = JSON.parse(JSON.stringify(this.classForm.value));
    values.class_date = this.dateService.dtToString(this.classForm.value.class_date);
    values.start_time = this.dateService.tmToString(this.classForm.value.start_time);
    values.end_time = this.dateService.tmToString(this.classForm.value.end_time);
    if (this.classForm.invalid) {
      return;
    }
    this.exerciseService.addExercise(values).subscribe(
      (data) => {
        this.classForm.reset();
        this.router.navigate(['/exercise', data.id]);
      },
      (error) => {
        this.notifier.notify('error', 'Ошибка при сохранений, попробуите еще раз');
      }
    );
  }

  selectGroup(group: Group) {
    this.selectedGroup.next(group);
    this.selectedGroupId.next(group.id);
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}
