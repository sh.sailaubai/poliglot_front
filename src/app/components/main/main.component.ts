import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth-service';
import {User} from '../../models/user';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  public user: User;
  private subscriptions: Subscription;
  public loading = true;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.subscriptions = this.authService.currentUser.subscribe(
      (user: User) => {
        this.user = user;
        this.loading = false;
      }
    );
  }

}
