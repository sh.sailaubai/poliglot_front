import { Component, OnInit, OnDestroy } from '@angular/core';
import { GroupService } from 'src/app/services/group-service';
import { Group } from 'src/app/models/group';
import { Student } from 'src/app/models/student';
import { Subscription } from 'rxjs';
import { DateService } from 'src/app/services/date.service';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.scss']
})
export class GroupListComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  private subscription2: Subscription;
  public loading = true;
  public group: Group;

  public groups: Group[];

  public students: Student[];
  month: number;
  year: number;

  monthsList: string[];
  yearsList: number[];

  dateModel: NgbDate;

  constructor(
    private groupService: GroupService,
    private dateService: DateService
  ) { }

  ngOnInit() {
    this.month = this.dateService.currentMonth();
    this.year = this.dateService.currentYear();
    this.dateModel = new NgbDate(this.year, this.month, 1);
    this.monthsList = this.dateService.monthsListRu;
    this.yearsList = this.dateService.availableYears;
    this.subscription = this.groupService.getListWithTeacher().subscribe((groups: Group[]) => {
      this.groups = groups.map((group: Group) => {
        this.dateService.weekList.forEach((w) => {
          group[w] = group[w] === "1"
        });
        return group;
      });
      this.loading = false;
    });
  }

  selectGroup(group: Group): void {
    this.group = group;
    this.loadStudents(group.id);
  }

  selectDate() {
    this.year = this.dateModel.year;
    this.month = this.dateModel.month;
    this.loadStudents(this.group.id);
  }

  loadStudents(groupId: number): void {
    this.students = [];
    this.subscription2 = this.groupService.getStudentsWithPayments(groupId, this.year, this.month).subscribe(
      (students: Student[]) => {
        students.forEach((students) => {
          students.payments.forEach((payment) => {
            payment.months_info = JSON.parse(payment.months_info)
          })
        });
        this.students = students;
      }
    )
  }

  ngOnDestroy(): void {
    if (this.subscription) this.subscription.unsubscribe();
    if (this.subscription2) this.subscription2.unsubscribe();
  }

}
