import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { DateService } from 'src/app/services/date.service';
import { GroupService } from 'src/app/services/group-service';
import { Group } from 'src/app/models/group';
import { Student } from 'src/app/models/student';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit, OnDestroy {
  @Input() groupId: Observable<number>;

  subscription: Subscription;
  subscription2: Subscription;
  nativeGroupId: number;
  group: Group;
  student: Student; 

  loading = true;

  constructor(
    private dateService: DateService,
    private groupService: GroupService
  ) { }

  ngOnInit() {
    this.subscription = this.groupId.subscribe((groupId) => {
      this.nativeGroupId = groupId;
      this.loading = true;
      this.group = null;
      this.loadGroup();
    });
  }

  loadGroup(): void {
    this.subscription2 = this.groupService.getWithStudents(this.nativeGroupId).subscribe((group) => {
      this.group = group;
      this.group.groupSchedule = this.dateService.groupScheduleList(this.group);
      this.group.students.forEach((student) => {
        student.group = this.group;
      });
      this.loading = false;
    });
  }

  selectStudent(student: Student): void {
    this.student = student;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.subscription2) {
      this.subscription2.unsubscribe();
    }
  }

}
