import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth-service';
import {AuthUser} from '../../models/user';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.scss']
})
export class AuthenticateComponent implements OnInit, OnDestroy {
  private nextUrl: string;
  public error: string;
  public loading = false;
  public authUser: AuthUser;

  public username: string;
  public password: string;
  private subscription: Subscription;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.nextUrl = this.route.snapshot.queryParams['next'] || '/';
  }

  login() {
    this.loading = true;
    this.subscription = this.authService.login(this.username, this.password).subscribe((user) => {
      this.router.navigate([this.nextUrl]);
    }, (err) => {
      this.error = 'Неправильный логин или пароль';
      this.loading = false;
    });
  }

  isValid() {
    return !(this.password && this.username);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
