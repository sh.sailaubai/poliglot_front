import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from './services/auth-service';
import {Observable, Subscription} from 'rxjs';
import {User} from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public user: User;
  private subscription: Subscription;

  constructor(
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.subscription = this.authService.currentUser.subscribe(
      (user) => this.user = user
    );
  }

  ngOnDestroy() {
    if (this.subscription) {this.subscription.unsubscribe()};
  }

  logout() {
    this.authService.logout();
  }
}
