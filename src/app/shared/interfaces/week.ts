export interface Week {
    name: string;
    short: string;
    value: number;
}
