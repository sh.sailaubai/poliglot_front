import {BaseApi} from '../shared/core/base-api';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Group} from '../models/group';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { Payment } from '../models/payment';
import { Today } from '../models/today';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseApi {

  getGroups(idTeacher: number): Observable<Group[]> {
    return this.get(`/users/${idTeacher}/groups`).pipe(map((group) => {
      this.dateService.weekList.forEach((w) => {
        group[w] = group[w] === "1"
      });
      return group;
    }));
  }

  getTeachers(): Observable<User[]> {
    return this.get(`/users/teachers`);
  }

  addTeacher(data: User): Observable<User> {
    return this.post(`/users`, data)
  }

  getPayments(idTeacher: number, year: number, month?: number): Observable<Payment[]> {
    return this.getWithParams(`/users/${idTeacher}/payments`, {year, month});
  }

  getGroupsWithExercises(idTeacher: number, year: number, month?: number): Observable<Group[]> {
    return this.getWithParams(`/users/${idTeacher}/group-exercises`, {year, month}).pipe(map((group) => {
      this.dateService.weekList.forEach((w) => {
        group[w] = group[w] === "1"
      });
      return group;
    }));
  }

  getToday(): Observable<Today> {
    return this.get('/users/today')
  }
}
