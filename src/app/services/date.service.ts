import { Injectable } from '@angular/core';
import * as moment from 'moment';
import {NgbDateStruct, NgbTimeStruct, NgbDate} from '@ng-bootstrap/ng-bootstrap';
import { Group } from '../models/group';
import { Week } from '../shared/interfaces/week';

@Injectable({
  providedIn: 'root'
})
export class DateService {
  weekList = ['sun', 'mo', 'tu', 'wed', 'th', 'fri', 'sat'];
  weekListRus: Week[] = [
    {
      name: 'Понедельник',
      short: 'mo',
      value: 1
    },
    {
      name: 'Вторник',
      short: 'tu',
      value: 2
    },
    {
      name: 'Среда',
      short: 'wed',
      value: 3
    },
    {
      name: 'Четверг',
      short: 'th',
      value: 4
    },
    {
      name: 'Пятница',
      short: 'fri',
      value: 5
    },
    {
      name: 'Суббота',
      short: 'sat',
      value: 6
    },
    {
      name: 'Воскресенье',
      short: 'sun',
      value: 0
    }
  ];

  monthsListRu = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ];

  availableYears = [2019, 2020];

  constructor() { }

  dtToString(dt: NgbDateStruct): string {
    return moment(dt.year + '-' + dt.month + '-' + dt.day, 'YYYY-M-D').format('YYYY-MM-DD');
  }

  yiiToMoment(date: string): moment.Moment {
    return moment(date, 'YYYY-MM-DD');
  }

  yiiToNative(date: string): Date {
    return this.yiiToMoment(date).toDate();
  }

  nativeToMoment(dt: Date): moment.Moment {
    return moment(dt)
  }

  tmToString(tm: NgbTimeStruct): string {
    return moment(tm.hour + '-' + tm.minute + '-' + tm.second, 'H-m-s').format('HH:mm:ss');
  }

  ngbDateToNative(dt: NgbDateStruct): Date {
    return moment(dt.year + '-' + dt.month + '-' + dt.day, 'YYYY-M-D').toDate();
  }

  ngbDateToMoment(dt: NgbDateStruct): moment.Moment {
    return moment(dt.year + '-' + dt.month + '-' + dt.day, 'YYYY-M-D');
  }

  momentToNgb(date: moment.Moment): NgbDate {
    const dtString = date.format('YYYY-M-D').split('-');
    return new NgbDate(Number(dtString[0]), Number(dtString[1]), Number(dtString[2]));
  }

  groupScheduleList(group: Group): number[] {
    return this.weekList.filter((i:string) => group[i]).map((i) => this.weekList.indexOf(i))
  }

  dateRange(startDate: NgbDateStruct, endDate: NgbDateStruct): Date[] {
    let dates = [];

    let currDate = moment(startDate.year + '-' + startDate.month + '-' + startDate.day, 'YYYY-M-D').startOf('day');
    let lastDate = moment(endDate.year + '-' + endDate.month + '-' + endDate.day, 'YYYY-M-D').startOf('day');

    while(currDate.diff(lastDate) <= 0) {
        dates.push(currDate.clone().toDate());
        currDate.add(1, 'days');
    }

    return dates;
  } 

  dateRangeInfo(startDate: NgbDateStruct, endDate: NgbDateStruct, groupScedule: number[], perDay: number): any {
    const days = {};
    let currDate = moment(startDate.year + '-' + startDate.month + '-' + startDate.day, 'YYYY-M-D').startOf('day');
    let lastDate = moment(endDate.year + '-' + endDate.month + '-' + endDate.day, 'YYYY-M-D').startOf('day');

    while(currDate.diff(lastDate) <= 0) {
      if (groupScedule.includes(currDate.day())) {
        if (days.hasOwnProperty(currDate.month()+1)) {
          days[currDate.month()+1] += Number(perDay);
        } else {
          days[currDate.month()+1] = Number(perDay);
        }
      }
      currDate.add(1, 'days');
    }
    return days;
  }

  groupDateRange(startDate: moment.Moment, endDate: moment.Moment, groupScedule: number[]): NgbDate[] {
    let dates = [];
    let currDate = startDate.clone().startOf('day');
    let lastDate = endDate.clone().startOf('day');

    while(currDate.diff(lastDate) <= 0) {
      if (groupScedule.includes(currDate.day())) {
        dates.push(this.momentToNgb(currDate.clone()));
      }
      currDate.add(1, 'days');
    }

    return dates;
  }

  currentMonth(): number {
    return moment().month() + 1;
  }

  currentYear(): number {
    return moment().year();
  }
}
