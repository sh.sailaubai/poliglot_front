import {BaseApi} from '../shared/core/base-api';
import {Injectable} from '@angular/core';
import {Group} from '../models/group';
import {Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DateService } from './date.service';
import { map } from 'rxjs/operators';
import { Exercise } from '../models/exercise';
import { Student } from '../models/student';

@Injectable()
export class GroupService extends BaseApi {

    getWithStudents(id: number): Observable<Group> {
        return this.get(`/groups/${id}/with-students`).pipe(map((group) => {
          this.dateService.weekList.forEach((w) => {
            group[w] = group[w] === "1"
          });
          return group;
        }
        ))
    }

    addGroup(group: Group): Observable<Group> {
      return this.post('/groups', group)
    }

    updateGroup(group: Group): Observable<Group> {
      return this.put(`/groups/${group.id}`, group)
    }

    getAll(): Observable<Group[]> {
      return this.get('/groups')
    }

    getListWithTeacher(): Observable<Group[]> {
      return this.get(`/groups/with-teacher`)
    }

    getExersices(id: number, page?: number): Observable<Exercise[]> {
      page = page || 1;
      return this.getWithParams(`/groups/${id}/exercises`, {page})
    }

    getStudentsWithPayments(id: number, year: number, month: number): Observable<Student[]> {
      return this.getWithParams(`/groups/${id}/student-payments`, {year, month})
    }
}
