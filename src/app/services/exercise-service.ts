import {BaseApi} from '../shared/core/base-api';
import {Injectable} from '@angular/core';
import {Exercise} from '../models/exercise';
import { Observable } from 'rxjs';
import { Payment } from '../models/payment';
import { Student } from '../models/student';
import { Group } from '../models/group';
import { map } from 'rxjs/operators';
import { DateService } from './date.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ExerciseService extends BaseApi {

  addExercise(exercise: Exercise) {
    return this.post('/exercises', exercise);
  }

  getExercise(id: number): Observable<Exercise> {
    return this.get(`/exercises/${id}`);
  }

  getStudents(id: number): Observable<Student[]> {
    return this.get(`/exercises/${id}/students`);
  }

  getGroup(id: number): Observable<Group> {
    return this.get(`/exercises/${id}/group`).pipe(map((group) => {
      this.dateService.weekList.forEach((w) => {
        group[w] = group[w] === "1"
      });
      return group;
    }
    ))
  }

}
