import {BaseApi} from '../shared/core/base-api';
import {Injectable} from '@angular/core';
import { Payment } from '../models/payment';
import { Observable } from 'rxjs';
import { PaymentMap } from '../models/payment-map';
import { DateService } from './date.service';
import { HttpClient } from '@angular/common/http';
import { Student } from '../models/student';

@Injectable()
export class StudentService extends BaseApi {

    addPayments(data: Payment): Observable<Payment> {
        return this.postWithParams('/payments', data);
    }

    getAll(): Observable<Student[]> {
        return this.get('/students')
    }

    getPayments(studentId: number): Observable<Payment[]> {
        return this.getWithParams(`/students/${studentId}/payments`, {sort: 'paid_from'})
    }

    addStudent(student: Student): Observable<Student> {
        return this.post('/students', student)
    }

    // paymentDateRange(payments: Payment[]): PaymentMap {
    //     const paymentMap = new PaymentMap();
    //     payments.forEach((payment) => {
    //         payment.paid_from_date = this.dateService.yiiToMoment(payment.paid_from);
    //         payment.paid_to_date = this.dateService.yiiToMoment(payment.paid_to);
    //     });
    //     payments.sort((el1: Payment, el2: Payment) => {
    //         return el1.paid_from_date.isBefore(el2.paid_to_date) ? 1 : el1.paid_from_date.isAfter(el2.paid_to_date) ? -1 : 0;
    //     });
    //     payments.forEach((payment: Payment) => {
    //         if (!paymentMap.dateFrom) {
    //             paymentMap.dateFrom = payment.paid_from_date.clone();
    //         }
    //         if (!paymentMap.dateTo) {
    //             paymentMap.dateTo = payment.paid_to_date.clone();
    //         }
    //         if (!paymentMap.redDays) {
    //             paymentMap.redDays = [];
    //         }
    //         if (payment.paid_from_date.isBetween(paymentMap.dateFrom, paymentMap.dateTo,  null, '[]') && payment.paid_to_date.isAfter(paymentMap.dateTo)) {
    //             paymentMap.dateTo = payment.paid_to_date.clone();
    //         } else if (!payment.paid_to_date.isBetween(paymentMap.dateFrom, paymentMap.dateTo,  null, '[]')) {

    //         }
    //     });
    //     return paymentMap;
    // }

    paymentDateRange(payments: Payment[]): PaymentMap {
        const paymentMap = new PaymentMap();
        payments.forEach((payment) => {
            payment.paid_from_date = this.dateService.yiiToMoment(payment.paid_from);
            payment.paid_to_date = this.dateService.yiiToMoment(payment.paid_to);
        });
        if (payments.length > 0) {
            paymentMap.dateFrom = payments[0].paid_from_date;
            paymentMap.dateTo = payments[payments.length - 1].paid_to_date;
            paymentMap.dateFromNgb = this.dateService.momentToNgb(paymentMap.dateFrom);
            paymentMap.dateToNgb = this.dateService.momentToNgb(paymentMap.dateTo);
        } else {
            paymentMap.dateFrom = null;
            paymentMap.dateTo = null;
            paymentMap.dateFromNgb = null;
            paymentMap.dateToNgb = null;
        }
        return paymentMap;
    }
}
