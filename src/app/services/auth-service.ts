import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../models/user';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/internal/operators';
import {Injectable} from '@angular/core';

@Injectable()
export class AuthService {
  private _isLoggedIn = new BehaviorSubject<boolean>(false);
  private _currentUser = new BehaviorSubject<User>(null);

  get isLoggedIn(): Observable<boolean> {
    if (localStorage.getItem('currentUser')) {
      this._isLoggedIn.next(true);
    }
    return this._isLoggedIn.asObservable();
  }

  get currentUser(): Observable<User> {
    if (this.getUser()) {
      this._currentUser.next(this.getUser());
    }
    return this._currentUser.asObservable();
  }

  constructor(private http: HttpClient, private router: Router) {}

  public post(url: string = '', data: any = {}): Observable<any> {
    return this.http.post(url, data)
      .pipe(map((response: Response) => response));
  }

  public login(username: string, password: string) {
    return this.post(`${environment.apiUrl}/users/login`, {username: username, password: password})
      .pipe(map((user: User) => {
        if (user && user.auth_key) {
          this._isLoggedIn.next(true);
          this._currentUser.next(user);
          localStorage.setItem('currentUser', JSON.stringify(user));
        }
        return user;
      }));
  }

  public logout() {
    localStorage.removeItem('currentUser');
    this._isLoggedIn.next(false);
    this._currentUser.next(null);
    this.router.navigate(['/login']);
  }

  public getUser(): User {
    const user = localStorage.getItem('currentUser');
    if (user) {
      return JSON.parse(user);
    }
    return null;
  }
}
